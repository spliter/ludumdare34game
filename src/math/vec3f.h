/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * vec3f.h
 *
 *  Created on: 12-04-2011
 *      Author: Spliter
 */

#ifndef vec3f_H_
#define vec3f_H_
#include "mathUtils.h"

struct vec3f
{
public:
	union
	{
		real32 me[3];
		struct
		{
			real32 x;
			real32 y;
			real32 z;
		};
	};

	vec3f& set(real32 _x,real32 _y,real32 _z);
	vec3f& set(const vec3f& b);
	vec3f& set(real32 _x);

	real32 length() const;
	real32 squareLength() const;

	vec3f& normalize();
	vec3f normalized() const;

	vec3f& perpendicular(const vec3f& b);
	vec3f perpendiculared(const vec3f& b) const;

	vec3f& scale(real32 scale);
	vec3f scaled(real32 scale) const;

	vec3f& stretch(real32 newLength);
	vec3f stretched(real32 newLength) const;

	real32 dot(const vec3f& b) const;

	vec3f& cross(const vec3f& b);
	vec3f crossed(const vec3f& b) const;

	vec3f& rotate(real32 rad,real32 ax,real32 ay,real32 az);
	vec3f rotated(real32 rad,real32 ax,real32 ay,real32 az) const;
	vec3f& rotate(real32 rad,const vec3f& axis);
	vec3f rotated(real32 rad,const vec3f& axis) const;

	vec3f& rotateDeg(real32 deg,real32 ax,real32 ay,real32 az);
	vec3f rotatedDeg(real32 deg,real32 ax,real32 ay,real32 az) const;
	vec3f& rotateDeg(real32 deg,const vec3f& axis);
	vec3f rotatedDeg(real32 deg,const vec3f& axis) const;

	vec3f& reflect(const vec3f& n);
	vec3f reflected(const vec3f& n) const;

	// piecewise operators
	bool operator==(const vec3f& rh) const;
	bool operator!=(const vec3f& rh) const;

	vec3f& operator*=(const vec3f& rh);
	vec3f& operator/=(const vec3f& rh);
	vec3f& operator+=(const vec3f& rh);
	vec3f& operator-=(const vec3f& rh);

	vec3f operator*(const vec3f& rh) const;
	vec3f operator/(const vec3f& rh) const;
	vec3f operator+(const vec3f& rh) const;
	vec3f operator-(const vec3f& rh) const;

	vec3f operator-() const;
	vec3f operator+() const;

	//real32 operators:

	vec3f& operator=(const real32& rh);
	vec3f& operator*=(const real32& rh);
	vec3f& operator/=(const real32& rh);

	vec3f operator*(const real32& rh) const;
	vec3f operator/(const real32& rh) const;
};

real32 getAngle(const vec3f& from, const vec3f& to);

inline vec3f _vec3f(real32 x,real32 y,real32 z)
{
	vec3f result;
	result.set(x,y,z);
	return result;
}

inline vec3f& vec3f::set(real32 _x,real32 _y,real32 _z)
{
	x=_x;y=_y;z=_z;
	return *this;
}

inline vec3f& vec3f::set(const vec3f& b)
{
	x=b.x;y=b.y;z=b.z;
	return *this;
}

inline vec3f& vec3f::set(real32 _x)
{
	x=_x;y=_x;z=_x;
	return *this;
}

inline real32 vec3f::length() const
{
	return sqrt(sqr(x)+sqr(y)+sqr(z));
}
inline real32 vec3f::squareLength() const
{
	return sqr(x)+sqr(y)+sqr(z);
}

inline vec3f& vec3f::normalize()
{
	real32 len=squareLength();
	if(len>ERROR_MARGIN)
	{
		len=(real32)(1.0/sqrt(len));
		x*=len;
		y*=len;
		z*=len;
	}
	else
	{
		x=1.0;
		y=0.0;
		z=0.0;
	}
	return *this;
}
vec3f vec3f::normalized() const
{
	vec3f result;
	real32 len=squareLength();
	if(len>ERROR_MARGIN)
	{
		len=(real32)(1.0/sqrt(len));
		result.set(x*len,y*len,z*len);
	}
	else
	{
		result.set(1.0,0.0,0.0);
	}
	return result;
}

vec3f& vec3f::perpendicular(const vec3f& b)
{
	return cross(b);
}

vec3f vec3f::perpendiculared(const vec3f& b) const
{
	return crossed(b);
}

vec3f& vec3f::scale(real32 scale)
{
	x*=scale;
	y*=scale;
	z*=scale;
	return *this;
}
vec3f vec3f::scaled(real32 scale) const
{
	vec3f result={x*scale,y*scale,z*scale};
	return result;
}

vec3f& vec3f::stretch(real32 newLength)
{
	normalize();
	return scale(newLength);
}
vec3f vec3f::stretched(real32 newLength) const
{
	vec3f tmp=normalized();
	return tmp.scaled(newLength);
}

real32 vec3f::dot(const vec3f& b) const
{
	return x*b.x+y*b.y+z*b.z;
}


vec3f& vec3f::cross(const vec3f& b)
{
	real32 cx=y*b.z-z*b.y;
	real32 cy=z*b.x-x*b.z;
	real32 cz=x*b.y-y*b.x;
	x=cx;
	y=cy;
	z=cz;
	return *this;
}

vec3f vec3f::crossed(const vec3f& b) const
{
	vec3f result = {y*b.z-z*b.y,
					z*b.x-x*b.z,
					x*b.y-y*b.x};
	return result;
}

vec3f& vec3f::rotate(real32 rad,real32 ax,real32 ay,real32 az)
{
	real32 sina=sin(rad);
	real32 cosa=cos(rad);
	real32 cosb=1.0f-cosa;

	real32 nx=	x*(ax*ax*cosb+cosa)+
				y*(ax*ay*cosb-az*sina)+
				z*(ax*az*cosb+ay*sina);

	real32 ny=	x*(ay*ax*cosb+az*sina)+
				y*(ay*ay*cosb+cosa)+
				z*(ay*az*cosb-ax*sina);

	real32 nz=	x*(az*ax*cosb-ay*sina)+
				y*(az*ay*cosb+ax*sina)+
				z*(az*az*cosb+cosa);
	x=nx;
	y=ny;
	z=nz;
	return *this;
}

vec3f vec3f::rotated(real32 rad,real32 ax,real32 ay,real32 az) const
{
	real32 sina=sin(rad);
	real32 cosa=cos(rad);
	real32 cosb=1.0f-cosa;

	real32 nx=	x*(ax*ax*cosb+cosa)+
				y*(ax*ay*cosb-az*sina)+
				z*(ax*az*cosb+ay*sina);

	real32 ny=	x*(ay*ax*cosb+az*sina)+
				y*(ay*ay*cosb+cosa)+
				z*(ay*az*cosb-ax*sina);

	real32 nz=	x*(az*ax*cosb-ay*sina)+
				y*(az*ay*cosb+ax*sina)+
				z*(az*az*cosb+cosa);

	vec3f result = {nx,ny,nz};
	return result;
}

vec3f& vec3f::rotate(real32 rad,const vec3f& axis)
{
	return rotate(rad,axis.x,axis.y,axis.z);
}

vec3f vec3f::rotated(real32 rad,const vec3f& axis) const
{
	return rotated(rad,axis.x,axis.y,axis.z);
}

vec3f& vec3f::rotateDeg(real32 deg,real32 ax,real32 ay,real32 az)
{
	return rotate((real32)(deg*DEG_TO_RAD_CONST),ax,ay,az);
}
vec3f vec3f::rotatedDeg(real32 deg,real32 ax,real32 ay,real32 az) const
{
	return rotated((real32)(deg*DEG_TO_RAD_CONST),ax,ay,az);
}
vec3f& vec3f::rotateDeg(real32 deg,const vec3f& axis)
{
	return rotate((real32)(deg*DEG_TO_RAD_CONST),axis.x,axis.y,axis.z);
}
vec3f vec3f::rotatedDeg(real32 deg,const vec3f& axis) const
{
	return rotated((real32)(deg*DEG_TO_RAD_CONST),axis.x,axis.y,axis.z);
}

vec3f& vec3f::reflect(const vec3f& n)
{
	*this=(*this-(n*2.0*(n.dot(*this))));
	return *this;
}
vec3f vec3f::reflected(const vec3f& n) const
{
	vec3f aux;
	aux=*this;

	return (*this-n*2.0*(n.dot(*this)));
}

// piecewise operators
bool vec3f::operator==(const vec3f& rh) const
{
	return fabs(rh.x-x)<ERROR_MARGIN && fabs(rh.y-y)<ERROR_MARGIN && fabs(rh.z-z)<ERROR_MARGIN;
}
bool vec3f::operator!=(const vec3f& rh) const
{
	return fabs(rh.x-x)>=ERROR_MARGIN || fabs(rh.y-y)>=ERROR_MARGIN || fabs(rh.z-z)>=ERROR_MARGIN;
}

vec3f& vec3f::operator*=(const vec3f& rh)
{
	x*=rh.x;
	y*=rh.y;
	z*=rh.z;
	return *this;
}
vec3f& vec3f::operator/=(const vec3f& rh)
{
	x/=rh.x;
	y/=rh.y;
	z/=rh.z;
	return *this;
}
vec3f& vec3f::operator+=(const vec3f& rh)
{
	x+=rh.x;
	y+=rh.y;
	z+=rh.z;
	return *this;
}
vec3f& vec3f::operator-=(const vec3f& rh)
{
	x-=rh.x;
	y-=rh.y;
	z-=rh.z;
	return *this;
}

vec3f vec3f::operator*(const vec3f& rh) const
{
	vec3f result = {x*rh.x,
					y*rh.y,
					z*rh.z};
	return result;
}
vec3f vec3f::operator/(const vec3f& rh) const
{
	vec3f result = {x/rh.x,
					y/rh.y,
					z/rh.z};
	return result;
}
vec3f vec3f::operator+(const vec3f& rh) const
{
	vec3f result = {x+rh.x,
					y+rh.y,
					z+rh.z};
	return result;
}
vec3f vec3f::operator-(const vec3f& rh) const
{
	vec3f result = {x-rh.x,
					y-rh.y,
					z-rh.z};
	return result;
}

vec3f vec3f::operator-() const
{
	vec3f result = {-x,
					-y,
					-z};
	return result;
}

vec3f vec3f::operator+() const
{
	return vec3f(*this);
}

//real32 operators:

vec3f& vec3f::operator=(const real32& rh)
{
	x=rh;
	y=rh;
	z=rh;
	return *this;
}
vec3f& vec3f::operator*=(const real32& rh)
{
	x*=rh;
	y*=rh;
	z*=rh;
	return *this;
}
vec3f& vec3f::operator/=(const real32& rh)
{
	x/=rh;
	y/=rh;
	z/=rh;
	return *this;
}

vec3f vec3f::operator*(const real32& rh) const
{
	vec3f result = {x*rh,y*rh,z*rh};
	return result;
}
vec3f vec3f::operator/(const real32& rh) const
{
	vec3f result = {x/rh,y/rh,z/rh};
	return result;
}

vec3f operator*(const real32& lh,const vec3f& rh)
{
	vec3f result = {rh.x*lh,rh.y*lh,rh.z*lh};
	return result;
}
vec3f operator/(const real32& lh,const vec3f& rh)
{
	vec3f result = {lh/rh.x,lh/rh.y,lh/rh.z};
	return result;
}

real32 getAngle(const vec3f& from, const vec3f& to)
{
	return acosf(from.dot(to));
}

#endif /* vec3f_H_ */
