/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * bitmap_font_manager.cpp
 *
 *  Created on: 7 kwi 2015
 *      Author: Miko Kuta
 */

#include "../shared/font_manager.h"
#include "../math/box2f.h"
#include "../math/vec2f.h"

#define FONT_CHAR_NUM 256
struct bitmap_font_info
{
	font_handle handle;
	image_handle fontTexture;
	vec2f charScale;
	box2f charBbox[FONT_CHAR_NUM];
};

#define FONT_STRUCTS() FONT_DEFS(FONT_STRUCT_DEF)
#define FONT_STRUCT_DEF(name, filename, imageResource) imageResource,
image_resource fontImageResources[Font::Count] = { FONT_STRUCTS()};
#undef FONT_STRUCT_DEF
#undef FONT_STRUCTS

#define FONT_STRUCTS() FONT_DEFS(FONT_STRUCT_DEF)
#define FONT_STRUCT_DEF(name, filename, imageResource) filename,
const char* fontFileNames[Font::Count] = { FONT_STRUCTS() };
#undef FONT_STRUCT_DEF
#undef FONT_STRUCTS

struct bitmap_font_manager: public font_manager
{
	static bitmap_font_manager* create(game_renderer* renderer, uint8* soundMemory, uint64 soundMemorySize);
	virtual font_handle getFont(font_resource font);
	virtual font_info getFontInfo(font_resource font);
	virtual font_render_mesh_state createTemporaryText(const char* text, real32 x, real32 y, real32 size, font_handle font, real32 r, real32 g, real32 b, real32 a);
	virtual font_render_mesh_state createStaticText(memory_manager* manager, const char* text, real32 x, real32 y, real32 size, font_handle font, real32 r, real32 g, real32 b, real32 a);

	bitmap_font_info fontInfo[Font::Count];
	game_renderer* renderer;
};

bitmap_font_manager* bitmap_font_manager::create(game_renderer* renderer, uint8* fontMemory, uint64 fontMemorySize)
{
	bitmap_font_manager* fontManager = new (fontMemory) bitmap_font_manager();
	fontMemory += sizeof(bitmap_font_manager);
	fontMemorySize -= sizeof(bitmap_font_manager);
	fontManager->renderer = renderer;
	return fontManager;
}

void TEMPGenerateFontCharBoxes(game_renderer* renderer, bitmap_font_info* font)
{
	int32 charColumns = 16;
	int32 charRows = 16;
	real32 charWidth = 1.0f / charColumns;
	real32 charHeight = 1.0f / charRows;

	image_info texInfo = renderer->getImageInfo(font->fontTexture);
	real32 ratio = 1.0f;
	if (texInfo.height > 0)
	{
		ratio = (real32) texInfo.width / (real32) texInfo.height;
	}

	font->charScale.set((real32) charColumns * ratio, (real32) charRows);

	for (int32 y = 0; y < charRows; y++)
	{
		for (int32 x = 0; x < charColumns; x++)
		{
			int id = x + y * charColumns;
			font->charBbox[id].set(x * charWidth, y * charHeight, (x + 1) * charWidth, (y + 1) * charHeight);
		}
	}
}

font_handle bitmap_font_manager::getFont(font_resource fontResource)
{
	font_handle result = 0;
	if (fontResource > 0 && fontResource < Font::Count)
	{
		if(fontInfo[fontResource].handle==0)
		{
			image_handle imageHandle = renderer->getImage(fontImageResources[fontResource]);
			if (imageHandle != 0)
			{
				//TODO:load the font description file
				fontInfo[fontResource].handle = fontResource;
				fontInfo[fontResource].fontTexture = imageHandle;
				TEMPGenerateFontCharBoxes(renderer, &fontInfo[fontResource]);
				result = fontInfo[fontResource].handle;
			}
			else
			{
				printf("FAILED TO LOAD IMAGE %d\n", fontImageResources[fontResource]);
			}
		}
		else
		{
			result = fontInfo[fontResource].handle;
		}
	}
	else
	{
		printf("FONT RESOURCE OUT OF RANGE %d\n", fontResource);
	}
	return result;
}

font_info bitmap_font_manager::getFontInfo(font_handle fontHandle)
{
	font_info result =
		{ };
	if (fontHandle > 0 && fontHandle < Font::Count)
	{
		result.handle = fontInfo[fontHandle].handle;
		result.image = fontInfo[fontHandle].fontTexture;
	}
	return result;
}

void writeTextToMesh(font_render_mesh_state *mesh, const char* text, real32 x, real32 y, real32 size, bitmap_font_info font)
{
	int32 stringLength = strlen(text);
	mesh->mesh.usedVertexCount = 0;
	real32 curX = x;
	real32 curY = y;
	mesh->boundingBox.minX = curX;
	mesh->boundingBox.minY = curY;
	struct vert2f
	{
		vec2f pos;
		vec2f uv;
	};

	real32 maxX = curX;
	real32 maxY = curY;

	int32 vertexStride = sizeof(vert2f) / sizeof(real32);
	real32 *meshVerts = mesh->mesh.vertices;
	for (int32 charIndex = 0; charIndex < stringLength; charIndex++)
	{
		char curChar = text[charIndex];
		//Note: we have it here and not only if the char is visible because if we want to modify the mesh data afterwards eg for animation or colouring it's easier to find the right vertices
		box2f charBox = font.charBbox[curChar];
		vec2f curPos =
			{ curX, curY };
		real32 charWidth = charBox.getWidth() * font.charScale.x * size;
		real32 charHeight = charBox.getHeight() * font.charScale.y * size;

//		vec2f *pos = mesh->pos2f+charIndex*6;
//		pos[0] = curPos;
//		pos[1] = curPos + _vec2f(0, charHeight);
//		pos[2] = curPos + _vec2f(charWidth, charHeight);
//		pos[3] = curPos;
//		pos[4] = curPos + _vec2f(charWidth, charHeight);
//		pos[5] = curPos + _vec2f(charWidth, 0);
//		vec2f *uv = mesh->texCoords2f+charIndex*6;
//		uv[0].set(charBox.v1.x, charBox.v1.y);
//		uv[1].set(charBox.v1.x, charBox.v2.y);
//		uv[2].set(charBox.v2.x, charBox.v2.y);
//		uv[3].set(charBox.v1.x, charBox.v1.y);
//		uv[4].set(charBox.v2.x, charBox.v2.y);
//		uv[5].set(charBox.v2.x, charBox.v1.y);

		vert2f *vert = (vert2f*) (meshVerts + charIndex * 6 * vertexStride);
		vert[0].pos = curPos;
		vert[1].pos = curPos + _vec2f(0, charHeight);
		vert[2].pos = curPos + _vec2f(charWidth, charHeight);
		vert[3].pos = curPos;
		vert[4].pos = curPos + _vec2f(charWidth, charHeight);
		vert[5].pos = curPos + _vec2f(charWidth, 0);
		vert[0].uv.set(charBox.v1.x, charBox.v1.y);
		vert[1].uv.set(charBox.v1.x, charBox.v2.y);
		vert[2].uv.set(charBox.v2.x, charBox.v2.y);
		vert[3].uv.set(charBox.v1.x, charBox.v1.y);
		vert[4].uv.set(charBox.v2.x, charBox.v2.y);
		vert[5].uv.set(charBox.v2.x, charBox.v1.y);

		if (curChar == '\n')
		{
			//TODO: replace this with proper line spacing
			curX = x;
			curY += size;
		}
		else if (curChar == ' ')
		{
			curX += size * 0.5f;
		}
		else if (curChar == '\t')
		{
			curX += size * 2;
		}
		else
		{
			curX += charWidth;
		}
		if (curX > maxX)
		{
			maxX = curX;
		}
		if (curY + charHeight > maxY)
		{
			maxY = curY + charHeight;
		}
	}

	mesh->boundingBox.maxX = maxX;
	mesh->boundingBox.maxY = maxY;

	mesh->mesh.usedVertexCount = mesh->mesh.totalVertexCount;
}

font_render_mesh_state bitmap_font_manager::createTemporaryText(const char* text, real32 x, real32 y, real32 size, font_handle fontHandle, real32 r, real32 g, real32 b, real32 a)
{
	font_render_mesh_state result =
		{ };
	if (fontHandle > 0 && fontHandle < Font::Count)
	{
		int32 stringLength = strlen(text);
		bitmap_font_info font = fontInfo[fontHandle];
		result.mesh = renderer->allocTemporaryMesh(stringLength * 6, font.fontTexture, true, RendererPosition2D, RendererColorNone, RendererTexCoords2D);
		result.mesh.color.set(r, g, b, a);
		writeTextToMesh(&result, text, x, y, size, font);
	}
	else
	{
		printf("FONT HANDLE OUT OF RANGE %d\n", fontHandle);
	}
	return result;
}

font_render_mesh_state bitmap_font_manager::createStaticText(memory_manager* manager, const char* text, real32 x, real32 y, real32 size, font_handle fontHandle, real32 r, real32 g, real32 b, real32 a)
{
	font_render_mesh_state result =
		{ };
	if (fontHandle > 0 && fontHandle < Font::Count)
	{
		int32 stringLength = strlen(text);
		bitmap_font_info font = fontInfo[fontHandle];
		result.mesh = renderer->allocStaticMesh(manager, stringLength * 6, font.fontTexture, true, RendererPosition2D, RendererColorNone, RendererTexCoords2D);
		writeTextToMesh(&result, text, x, y, size, font);
		result.mesh.color.set(r, g, b, a);
	}
	else
	{
		printf("FONT HANDLE OUT OF RANGE %d\n", fontHandle);
	}
	return result;
}
