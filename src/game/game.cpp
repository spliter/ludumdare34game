/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
/*
 * game.cpp
 *
 *  Created on: 25 gru 2014
 *      Author: Miko Kuta
 */
#ifndef GAME_CPP
#define GAME_CPP
#include "game.h"
#include "camera.h"
#include "../shared/resources.h"
#include "../math/box2f.h"
#include "../math/mathUtils.h"
#include "../math/mathUtils.cpp"
#include "../math/random.h"
#include "../math/vec2f.h"

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

struct game_state
{
	font_handle debugFont;
	memory_manager gameMemory;
	random_state random;
	vec2f prevMousePos;
	game_camera_2d worldCamera;
	game_camera_2d uiCamera;
	image_handle cellSprite;
};

GAME_UPDATE_AND_RENDER(GAME_UPDATE_AND_RENDER_NAME)
{
	game_state *gameState = (game_state*) (memory->gameMemory);
	game_renderer *renderer = memory->renderer;
	game_renderer *debugRenderer = memory->debugRenderer;
	font_manager *fontManager = memory->fontManager;

	if (!memory->isInitialized)
	{
		memory->isInitialized = true;

		gameState->gameMemory.memory = memory->gameMemory + sizeof(game_state);
		gameState->gameMemory.memorySize = memory->gameMemorySize - sizeof(game_state);
		gameState->gameMemory.used = 0;

		gameState->debugFont = fontManager->getFont(Font::TestFont);

		gameState->uiCamera.anchor.set(0.0f, 0.0f);
		gameState->uiCamera.zoom = 1.0f;
		gameState->uiCamera.viewport.x1 = 0;
		gameState->uiCamera.viewport.x2 = (real32) screen->width;
		gameState->uiCamera.viewport.y1 = 0;
		gameState->uiCamera.viewport.y2 = (real32) screen->height;

		gameState->worldCamera.anchor.set(0.5f, 0.5f);
		gameState->worldCamera.zoom = 1.0f;
		gameState->worldCamera.viewport.x1 = 0;
		gameState->worldCamera.viewport.x2 = (real32) screen->width;
		gameState->worldCamera.viewport.y1 = 0;
		gameState->worldCamera.viewport.y2 = (real32) screen->height;

		gameState->cellSprite = renderer->getImage(Image::Cell);

	}

	real32 worldLeft = (real32) -screen->width * 0.5f;
	real32 worldTop = (real32) -screen->height * 0.5f;
	real32 worldRight = worldLeft + screen->width;
	real32 worldBottom = worldTop + screen->height;

	gameState->worldCamera.startCamera(debugRenderer);

	if (input->buttonMouseRight.wasPressed() && input->buttonMouseRight.isDown)
	{
		gameState->prevMousePos.x = input->mouseX;
		gameState->prevMousePos.y = input->mouseY;
	}
	else if (input->buttonMouseRight.isDown)
	{
		gameState->worldCamera.pos.x += (gameState->prevMousePos.x - input->mouseX) / gameState->worldCamera.zoom;
		gameState->worldCamera.pos.y += (gameState->prevMousePos.y - input->mouseY) / gameState->worldCamera.zoom;

		gameState->prevMousePos.x = input->mouseX;
		gameState->prevMousePos.y = input->mouseY;
	}

	gameState->uiCamera.viewport.set(0, 0, (real32) screen->width, (real32) screen->height);
	gameState->worldCamera.viewport.set(0, 0, (real32) screen->width, (real32) screen->height);

	gameState->worldCamera.startCamera(renderer);
	renderer->clearView(0.0f, 0.0f, 0.0f);

	gameState->uiCamera.startCamera(renderer);

	font_render_mesh_state helpTextMesh = fontManager->createTemporaryText("TEST", 100, 100, 32.0f, gameState->debugFont, 1.0f, 1.0f, 1.0f, 1.0f);
	renderer->drawMesh(helpTextMesh.mesh);
}

#endif

