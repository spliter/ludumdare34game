/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * game_utils.h
 *
 *  Created on: 31 mar 2015
 *      Author: Miko Kuta
 */

#ifndef GAME_UTILS_H_
#define GAME_UTILS_H_

#include "float.h"
#include "../shared/renderer.h"
#include <stdio.h>

struct game_camera_2d
{
	vec2f pos;
	real32 zoom;
	rectf viewport;
	vec2f anchor;
	real32 rotationDeg;

	void startCamera(game_renderer* renderer);
	vec2f cameraToWorldSpace(real32 x, real32 y);
	real32 cameraToWorldRotation(real32 rotationDeg);
};

struct game_camera_3d
{
	vec3f pos;
	real32 rotPitch,rotYaw;
	real32 fov;
	real32 nearClip, farClip;
	rectf viewport;

	quaternionf getRotationQuat();
	void startCamera(game_renderer* renderer);
	vec3f cameraToWorldSpace(real32 x, real32 y);
	quaternionf cameraToWorldRotation(quaternionf rot);
	//TODO: worldToCameraSpace and worldToCameraRotation
};

inline bool32 isInside(int32 testX, int32 testY, int32 rectX1, int32 rectY1, int32 rectX2, int32 rectY2)
{
	bool32 result = false;
	if (rectX1 <= testX && rectX2 > testX &&
		rectY1 <= testY && rectY2 > testY)
	{
		result = true;
	}
	return result;
}

inline void game_camera_2d::startCamera(game_renderer* renderer)
{
	renderer->setView2D(viewport.x1, viewport.y1, viewport.x2, viewport.y2, anchor.x, anchor.y);
//	renderer->pushMatrix(-x, -y, zoom, zoom, -rotationDeg);
	renderer->setTransform(0, 0, zoom, zoom, 0);
	renderer->mulTransform(0, 0, 1.0f, 1.0f, -rotationDeg);
	renderer->mulTransform(-pos.x, -pos.y, 1.0f, 1.0f, 0);
}

vec2f game_camera_2d::cameraToWorldSpace(real32 x, real32 y)
{
	vec2f result = { };
	vec2f viewportSize = _vec2f(viewport.x2 - viewport.x1, viewport.y2 - viewport.y1);
	result = _vec2f(x, y) - _vec2f(viewport.x1, viewport.y1) - viewportSize * anchor;
	result /= zoom;
	result.rotateDeg(rotationDeg);
	result += pos;
	return result;
}

real32 game_camera_2d::cameraToWorldRotation(real32 rotationDeg)
{
	real32 result = this->rotationDeg + rotationDeg;
	return result;
}

inline quaternionf game_camera_3d::getRotationQuat()
{
	quaternionf result = quaternionf::identity();
	result.rotateByDeg(rotYaw,_vec3f(0,1,0));
	result.rotateByDeg(rotPitch,_vec3f(1,0,0));
	return result;
}

inline void game_camera_3d::startCamera(game_renderer* renderer)
{
	quaternionf rot = getRotationQuat();
	renderer->setView3D(viewport.x1, viewport.y1, viewport.x2, viewport.y2, fov, nearClip, farClip);
	renderer->setTransform3D(0, 0, 0, 1.0f, 1.0f, 1.0f, rot.conjugate());
	renderer->mulTransform3D(-pos.x, -pos.y, -pos.z, 1.0f, 1.0f, 1.0f);
}

vec3f game_camera_3d::cameraToWorldSpace(real32 x, real32 y)
{
	//TODO: make sure this works with different aspect ratios
	vec3f result = { };
	vec2f viewportSize = _vec2f(viewport.x2 - viewport.x1, viewport.y2 - viewport.y1);
	vec2f viewSpacePos = { x / viewportSize.x,
							-y / viewportSize.y};
	viewSpacePos.x-=0.5f;
	viewSpacePos.y+=0.5f;

	viewSpacePos.x*=(viewportSize.x/viewportSize.y);
	real32 scale = sin(degToRad(fov*0.5f));
	result.set(viewSpacePos.x*scale,viewSpacePos.y*scale,-1);
//	printf("scale=%f\nsize=(%f,%f)\n",scale,viewSpacePos.x,viewSpacePos.y);

	quaternionf rot = getRotationQuat();
	result = rot.getRotationMatrix() * result;
	result += pos;
	return result;
}

quaternionf game_camera_3d::cameraToWorldRotation(quaternionf rotation)
{
	quaternionf rot = getRotationQuat();
	quaternionf result = rot*rotation;
	return result;
}
#endif
