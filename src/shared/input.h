/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * input.h
 *
 *  Created on: 3 cze 2015
 *      Author: Miko Kuta
 */

#ifndef INPUT_H_
#define INPUT_H_
#include "types.h"
#include "utils.h"
struct button_state
{
	int32 switchCount;
	bool32 isDown;

	inline bool32 wasPressed()
	{
		bool32 result = false;
		result = (isDown && switchCount==1) || switchCount>=2;
		return result;
	}

	inline bool32 isPressed()
	{
		bool32 result = false;
		result = isDown;
		return result;
	}

	inline bool32 wasReleased()
	{
		bool32 result = false;
		result = (!isDown && switchCount==1) || switchCount>2;
		return result;
	}
};

#define BUTTON_DEFS(name)\
		name(buttonMouseLeft)\
		name(buttonMouseMiddle)\
		name(buttonMouseRight)\
		name(buttonZoomIn)\
		name(buttonZoomOut)\
		name(buttonSpeedUp)\
		name(buttonSlowDown)\
		name(buttonSelectNext)\
		name(buttonSelectPrev)\
		name(buttonNextOp)\
		name(buttonStopOnGeneration)\
		name(buttonToggleHelp)\
		name(buttonToggleDebug)\
		name(buttonToggleInfo)\
		name(buttonToggleGraphics)

#define BUTTON_ENUMS()\
		BUTTON_DEFS(BUTTON_ENUM_DEF)
#define BUTTON_ENUM_DEF(name) gi##name,

#define BUTTON_STRUCTS()\
		BUTTON_DEFS(BUTTON_STRUCT_DEF)
#define BUTTON_STRUCT_DEF(name) button_state name;

#define BUTTON_STRINGS()\
		BUTTON_DEFS(BUTTON_STRING_DEF)
#define BUTTON_STRING_DEF(name) STRINGIZE(name),

enum game_input_button
{
	BUTTON_ENUMS()
	giButtonCount
};

const char* giButtonNames[]={
	BUTTON_STRINGS()
};

struct game_input
{
	real32 mouseX, mouseY;

	union
	{
		button_state buttons[giButtonCount];
		struct
		{
			BUTTON_STRUCTS()
		};
	};
};



#endif /* INPUT_H_ */
