/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * game_common.h
 *
 *  Created on: 25 gru 2014
 *      Author: Miko Kuta
 */

#ifndef UTILS_H
#define UTILS_H
#include "types.h"
#include "string.h"

#define Assert(condition) if(!(condition)){*((int*)(0))=0;}

#define Kilobytes(n) ((n)*1024L)
#define Megabytes(n) (Kilobytes(n)*1024L)
#define Gigabytes(n) (Megabytes(n)*1024L)

#define ToKilobytes(n) ((n)/1024L)
#define ToMegabytes(n) (ToKilobytes(n)/1024L)
#define ToGigabytes(n) (ToMegabytes(n)/1024L)

#define STRINGIZEEX(value) #value
#define STRINGIZE(value) STRINGIZEEX(value)

#define MACRO_PROTECT(...) __VA_ARGS__

#define local static
#define global_variable static
#define persistent static

#define ZERO_STRUCT(struct_class,ptr) memset(ptr,0,sizeof(struct_class))
#define ZERO_VARIABLE(var) memset(&var,0,sizeof(var))

struct memory_manager
{
	uint8* memory;
	uint64 memorySize;
	uint64 used;
};

#define pushStruct(manager,struct) (struct*)pushMemory(manager,sizeof(struct))
#define pushStructArray(manager,struct, size) (struct*)pushMemory(manager,sizeof(struct)*size)

#ifndef NULL
#define NULL 0
#endif

uint8* pushMemory(memory_manager* manager,uint32 size, uint32 alignment = 4)
{
	uint8* result = NULL;
	uint32 newStart = (manager->used+alignment-1)&~(alignment-1);
	Assert(newStart+size<manager->memorySize);
	result = manager->memory+newStart;
	manager->used+=size;
	return result;
}

#endif
